// Setting up dependencies
const express = require('express');
const router = express.Router();

// Importing the "user" controller from the "controllers" folder
const userController = require('../controllers/user');

// Importing the auth middleware
const auth = require('../auth');

// SETTING UP THE ROUTERS

// Route for checking if user email already exists in the database
router.post('/checkemail', (req, res) => {
    // Use the checkEmailExists method from the "userController" to check if the email exists which passes the body of the request (req.body) to the corrensponding controller
    userController.checkEmailExists(req.body).then((resultFromController) => {
        res.send(resultFromController);
    });
});

// Route for registering a user
router.post('/register', (req, res) => {
    // Use the registerUser method from the "userController" to register a user which passes the body of the request (req.body) to the corrensponding controller
    userController.registerUser(req.body).then((resultFromController) => {
        res.send(resultFromController);
    });
});

// S38 ACTIVITY
// Route for retrieving user details
router.get('/details', auth.verify, (req, res) => {
    // Use the "decode" method from the "auth" middleware to get the user details from the token passing the "req" object
    const userData = auth.decode(req.headers.authorization);
    // Use the getProfile method from the "userController" to get the user details
    userController.getProfile({ userId: req.body.id})
        .then((resultFromController =>
            res.send(resultFromController))
    );
});


// S41 ACTIVITY
// Refactoring user route for to implement authentication for the enroll route
router.get('/enroll', auth.verify, (req, res) => {
    // Use the "decode" method from the "auth" middleware to get the user details from the token passing the "req" object
    const userData = auth.decode(req.headers.authorization);
    // Use the "enroll" method from the "userController" to enroll the user to the course
    userController.enroll({ userId: userData.id, courseId: req.body.courseId })
        .then((resultFromController =>
            res.send(resultFromController))
    );
});

// Allows export of the "user" router to be used in the "index.js" file
module.exports = router;