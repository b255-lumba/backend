// Setting up dependencies
const express = require('express');
const mongoose = require('mongoose');

// Setting up the app
const app = express();

// Setting up the database connection
mongoose.connect('mongodb+srv://jlumbajrph:Admin.123@test.ezjezum.mongodb.net/?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

mongoose.connection.once('open', () => {
    console.log('Now connected to MongoDB Atlas');
});

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Setting up the routes

// Importing the "user" route from the "routes" folder
app.use('/users', userRoutes);

// Importing the "course" route from the "routes" folder
app.use('/courses', courseRoutes);

// Setting up the server and listening to port
if (require.main === module) {
    app.listen(process.env.PORT || 4000, () => {
        console.log(`API is now online on port ${process.env.PORT || 4000}`)
    });
};

// Exporting the app
module.exports = app;