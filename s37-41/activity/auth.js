// Setting up the authentification middleware

const jwt = require('jsonwebtoken');


const secret = "CourseBookingAPI";

// Creating a token
module.exports.createAccessToken = (user) => {
    // The data will be received from a registration form. When the user logs in, a token will be generated with the user details
    const data = {
        id: user._id,
        email: user.email,
        firstName: user.firstName,
        isAdmin: user.isAdmin
    };
    // The token will be generated using the "jwt.sign" method utilizing the form data and secret code with no additional options provided (will be valid for 1 hour - optional)
    return jwt.sign(data, secret, { expiresIn: '1h' });
};

// Verifying the token
module.exports.verify = (req, res, next) => {
    // The token is retrieved from the request header
    // This can be provided in the Postman under Authorization > Bearer Token
    let token = req.headers.authorization;

    // Token is received and is not undefined
    if (typeof token !== "undefined") {
        console.log(token);
        // The token sent is a type of "bearer token" which when received contains the word "bearer" as prefix to the token string
        // Slice the "Bearer" part from the token
        token = token.slice(7, token.length);

        // Verify the token using the "jwt.verify" method, this will decrypt the token using the secret code
        return jwt.verify(token, secret, (err, data) => {
            // if there is an error, return an error message
            if (err) {
                return res.send({auth: 'failed'});
            } else {
                // If there is no error, hence the JWT is valid, proceed to the next middleware function by calling the "next" method
                next();
            }
        });
    } else {
        // If the token does not exit, return failed authentication
        return res.send({auth: 'failed'});
    }
};

// Decoding/decrypting the token
module.exports.decode = (token) => {
    // If the token received is not defined, slice the "Bearer" part from the token and verify the token using the "jwt.verify" method, this will decrypt the token using the secret code
    if (typeof token !== "undefined") {
        token = token.slice(7, token.length);
        return jwt.verify(token, secret), (err, data) => {
            // if there is an error, return null
            if (err) {
                return null;
            } else {
                // If there is no error, hence the JWT is valid, decode using the "jwt.decode" method and complete: true to return the full decoded token and return the payload
                return jwt.decode(token, {complete: true}.payload);
            }
        };
        // Token does not exist, return null
    } else {
        return null;
    }
};
    
