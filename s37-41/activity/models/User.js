// Dependencies
const moongose = require('mongoose');

// Schema
const userSchema = new moongose.Schema({
    firstName: {
        type: String,
        // Make sure it is required and would return a message "First name is required"
        required: [true, "First name is required"]
    },
    lastName: {
        type: String,
        required: [true, "Last name is required"]
    },
    email: {
        type: String,
        required: [true, "Email is required"]
    },
    password: {
        type: String,
        required: [true, "Password is required"]
    },
    isAdmin : {
        type: Boolean,
        default: false
    },
    mobileNo: {
        type: String,
        required: [true, "Mobile number is required"]
    },
    // enrollments - array of objects
    enrollments: [
        {
            courseId: {
                type: String,
                required: [true, "Course ID is required"]
            },
            enrolledOn: {
                type: Date,
                default: new Date()
            },
            status: {
                type: String,
                default: "Enrolled"
            }
        }
    ]
});

// Allow export of the model
module.exports = moongose.model('User', userSchema);