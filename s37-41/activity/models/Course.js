// SETTING THE COURSE MODEL
// Setting up dependencies
const mongoose = require('mongoose');

// Setting up the schema
const courseSchema = new mongoose.Schema({
    name : {
        type: String,
        // Required and would return a message "Course name is required"
        required: [true, "Course name is required"]
    },
    description : {
        type: String,
        required: [true, "Course description is required"]
    },
    isActive : {
        type: Boolean,
        default: true
    },
    createdOn : {
        type: Date,
        // The new Date() method instantiates a new date that stores the current date and time whenever a new course is created in our database
        default: new Date()
    },
    // enrollee - array of objects containing userId and enrolledOn
    enrollee: [
        {
            userId: {
                type: String,
                required: [true, "User ID is required"]
            },
            enrolledOn: {
                type: Date,
                default: new Date()
            }
        }
    ]
});

// Allow export of the model
module.exports = mongoose.model('Course', courseSchema);