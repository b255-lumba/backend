// Dependencies
// "User" variable is capitalized to indicate that it is a model
const User = require('../models/User');
const Course = require('../models/Course');
const bcrypt = require('bcrypt');
const auth = require('../auth');

// CONTROLLER FUNCTIONS

// Checking if email already exists
module.exports.checkEmailExists = (reqBody) => {
    // The result is sent back to the frontend via the "then" method in the "userRoutes" file using the mongoose find() method
    return User.find({ email: reqBody.email }).then((result) => {
        // If the result is not empty, the email already exists
        if (result.length > 0) {
            return true;
            // if not, the user is not yet registered in the database
        } else {
            return false;
        }
    });
};

// Registering a new user
module.exports.registerUser = (reqBody) => {
    // Create a variable "newUser" to store the new user details and instantiate the "User" model
    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        // Hash the password using the bcrypt library
        password: bcrypt.hashSync(reqBody.password, 10)
    });

    // Save the new user to the database
    return newUser.save().then((user, error) => {
        // If there is an error, return false
        if (error) {
            return false;
        } else {
            return true;
        }
    });
};

// Logging in a user & authenticating the user
module.exports.loginUser = (reqBody) => {
    // Find the user by the email using the findOne() method
    return User.findOne({ email: reqBody.email }).then(result => {
        // If the user is not found, return false
        if (result === null) {
            return false;
        } else {
            // If the user is found, compare the password using the bcrypt library by creating a variable isPasswordCorrect and passing the password from the request body and the password from the database
            let isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

            // If the password is correct/match, generate a token using the "createAccessToken" method from the "auth" middleware and pass the user details then return the object to the frontend
            if (isPasswordCorrect) {
                return {access : auth.createAccessToken(result)};
            } else {
                // when passwords don't match, return false
                return false;
            }
        }
    });
};

// Getting the details of user profile
module.exports.getProfile = (data) => {
    // Find the user by the ID
    return User.findById(data.userId).then(result => {
        // If the user is found, return the user details and reassign the password to empty string to prevent it from being sent to the client and be exposed
        if (result === null) {
            return false;
        } else {
            result.password = '';
            return result;
        }
})
};

// Enroll user to a class
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Atlas Database

*/
// Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user
module.exports.enroll = async (data) => {
	// Add the course ID in the enrollments array of the user
	// Creates an "isUserUpdated" variable and returns true upon successful update otherwise false
	// Using the "await" keyword will allow the enroll method to complete updating before returing a response back to the frontend
	let isUserUpdated = await User.findById(data.userId).then(user => {

		// Adds the courseId in the user's enrollments array
		return user.save().then((user, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		})
	})

	// Add the user ID in the enrollees array of the course
	// Using the "await" keyword will allow the enroll method to complete updating the course before returning a response back to the frontend
	let isCourseUpdated = await Course.findById(data.courseId).then(course =>{

		// Adds the userId in the course's enrollees array
		course.enrollees.push({userId : data.userId});

		// Saves the updated course information in the database
		return course.save().then((course, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		})
	})

	// Condition that will check if the user and course documents have been updated
	// User enrollment successful
	if(isUserUpdated && isCourseUpdated){
		return true;
	// User enrollment failure
	} else {
		return false
	}
};

