const express = require("express");

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended:true}));

app.get("/", (req , res) => {
    res.send("Welcome to the home page");
});

// create a mock database with array containing object with usernames inside
let users = [];


app.get("/users", (req, res) => {
    res.send(users);
    // users.push(req.body);
    // res.send([req.body]);
})

app.delete("/delete-user", (req, res) => {
    let message;
    // create a condition to check if there are users found in the array
    if (users.length > 0) {
        // if there are users in the array, loop through the array of objects
        for (let i = 0; i < users.length; i++) {
            // if the username provided in the Postman client and the username of the current object in the loop is the same
            if (req.body.userName == users[i].userName) {
                // remove the user from the array, use splice array method
                users.splice(i, 1);
                message = `User ${req.body.userName} has been deleted`;
                break;
            }
        }
        // Outside the loop, add an if statement that will check if the message variable is undefined, update the message variable to "User does not exist"
        if (message === undefined) {
            message = "User does not exist";
        } else if (users.length === 0) {
            message = "No users found";
        }
    } else {
        message = "No users found";
    }
    res.send(message);
});


if(require.main === module) {
	app.listen(port, () => console.log(`Server running at port ${port}`));
}

module.exports = app;

