const http = require('http');

const port = 4000;

const server = http.createServer((req, res) => {
    if (req.url == "/") {
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end('Welcome to Booking System');
    } else if (req.url == "/profile") {
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end('Welcome to your Profile!');
    } else if (req.url == "/courses") {
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end("Here's our available courses");
    } else if (req.url == "/addcourse") {
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end('Add a course to our resources');
    } else if (req.url == "/updatecourse") {
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end('Update a course to our resources');
    } else if (req.url == "/archivecourses") {
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end('Archive course to our resources');
    } else {
        res.writeHead(404, {'Content-Type': 'text/plain'});
        res.end('404 - Page not Found');
    }
});

server.listen(port);

console.log(`Server now accessible at localhost:${port}`);

// Reminder to self:
// Server is created. To start it, go to gitbash and type: node <filename>
// gitbash console should show the console log: Server now accessible at localhost:4000
// Test endpoints in Postman