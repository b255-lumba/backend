const express = require('express');

const mongoose = require('mongoose');

const app = express();

const port = 3005;

mongoose.connect('mongodb+srv://jlumbajrph:Admin.123@test.ezjezum.mongodb.net/?retryWrites=true&w=majority',
    {
        useNewUrlParser: true, 
        useUnifiedTopology: true 
    }
);

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log('Cloud database connected'));

const userSchema = new mongoose.Schema({
    username: String,
    password: String
});

const User = mongoose.model('User', userSchema);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// POST ROUTE for /signup route to create a user
app.post('/signup', (req, res) => {
    // add functionality to check if there are duplicate users, if so, return an error using res.send to contain "Duplicate username found"
    User.findOne({username: req.body.username, password: req.body.password}).then((result, err) => {
        if (result != null && result.username == req.body.username && result.password == req.body.password) {
            // adding return before res.send will stop the function from executing further
            return res.send("Duplicate username found");
        } else {
            // add an if statement to check if username and password is not empty then instantiate a new user and save it to the database
            if (req.body.username != "" && req.body.password != "") {
                let newUser = new User({
                    username: req.body.username,
                    password: req.body.password
                });
                
                newUser.save().then((savedUser, saveErr) => {
                    if (saveErr) {
                        // print any errors in the console
                        return console.error(saveErr);
                    } else {
                        // return a status code of 201 for created and send 'New User registered' as the response
                        return res.status(201).send('New User registered');
                    }
                });
            } else {
                // return a status code of 400 for bad request and send 'BOTH username and password must be provided' as the response
                return res.status(400).send('BOTH username and password must be provided');
            }
        }
    });
});

if (require.main === module) {
    app.listen(port, () => console.log(`Server running on port ${port}`));
};

module.exports = app;

  