console.log("Hello friend");

// Array Methods

// Javascript has built-in functions and methods for arrays. This allows us to manipulate and access array items.

// Mutator Methods

/*
	- Mutator Methods are functions that "mutate" or change an array aftehr they're created
	- These methods manipulate the original array performing various tasks such as adding and removing elements.
*/

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

// push() method - adds an element in the end of an array AND returns the array's length

console.log("Current array:");
console.log(fruits);
let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);
console.log("Mutated array from push method:");
console.log(fruits);

// Adding multiple elements to an array
fruits.push('Avocado', 'Guava');
console.log("Mutated array from push method:");
console.log(fruits);

// pop() method - removes the last element in an array AND returns the removed element

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop method:");
console.log(fruits);

// unshift() method - adds an element at the beginning of an array

fruits.unshift('Lime', 'Banana');
console.log(fruits);

// shift() methiod - removes an element at the beginning of an array AND returns the removed element

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method:");
console.log(fruits);

// splice() method - simultaneously removes elements from and adds elements

fruits.splice(1, 2, 'Lime', 'Cherry');
console.log("Mutated array from splice method:");
console.log(fruits);

// sort() method - rearranges the array elements in alphanumeric order. Disadvantage: Only checks the first character of an element, i.e, will sort 2020 first before the number 3. Result = [2020, 3]

fruits.sort();
console.log("Mutated array from sort method:");
console.log(fruits);

// reverse() method - reverses the order of array elements

fruits.reverse();
console.log("Mutated array from reverse method:")
console.log(fruits);


// NON-MUTATOR METHODS
/*
	- Non-mutator mehtods are functiosn that do not modify or change an array after they're created
	- These methods do not manipulate original array performing various tasks sucj as returning elements from an array or combining arrays
*/

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

/*
	indexOf() method
	- returns the index of the first matching element found in an array
	- if no match was found, the result will be -1
	- the search process will be done from first element proceeding to the last element
*/

let firstIndex = countries.indexOf('PH');
console.log("Result of indexOf method: " + firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log("Result of indexOf method: " + invalidCountry);

/*
	lastIndexOf() method
	- returns the index of the last matching element found in an array
	- the search process will be done from last element proceeding to the first element
*/

let lastIndex = countries.lastIndexOf('PH');
console.log("Result of lastIndexOf method: " + lastIndex);

let lastIndexStart = countries.lastIndexOf('PH', 6);
console.log("Result of lastIndexOf method: " + lastIndexStart);


// slice() method - portions/slice elements from an array AND returns a new array

let slicedArrayA = countries.slice(2);
console.log("Result from slice method: ")
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 4);
console.log("Result from slice method: ")
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-1); // play with argument
console.log("Result from slice method: ")
console.log(slicedArrayC);