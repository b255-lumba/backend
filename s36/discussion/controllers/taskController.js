// Controllers contain the functions and business logic of our express.js application
// Meaning all the operations it can do will be placed in this file

// Uses the "require" directive to allow access to the Task model which allows us to access mongoose methods to perform CRUD operations
// Allows us to use the contents of the "task.js" file in the "models" folder
const Task = require("../models/task");

// Controller function for getting all the tasks
// Defines the functions to be used in the "taskRoute.js" file and exports these functions
module.exports.getAllTasks = () => {
	// The "return" statement returns the result of the  mongoose method "find" back to the "taskRoute.js" file which invokes this function when the "/tasks" route is accessed
	return Task.find({}).then(result => {
		return result;
	})
}

// Controller function for creating a task
// The request body coming from the client was passed from the "taskRoute.js" file via the "req.body" as an argument and is renamed as a "requestBody" parameter in the controller file
module.exports.createTask = (requestBody) => {
	let newTask = new Task({

		// Sets the "name" property with the value received from the client/Postman
		name: requestBody.name
	})

	// Saves the newly created "newTask" object in the MongoDB database
	// The "then" method waits until the task is stored in the database or an error is encountered before returning a "true" or "false" value back to the client/postman

	return newTask.save().then((task, error) => {
		if (error) {
			console.log(error);
			return false;
		}
	}
}