// Creating the Schema, model and then exporting the model to be used in the other files
const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({
    name: String,
    status: {
        type: String,
        default: 'pending'
    }
});

// module.exports is a way for Node.js to expose the Task model to other files as a "package" (module)
module.exports = mongoose.model('Task', taskSchema);