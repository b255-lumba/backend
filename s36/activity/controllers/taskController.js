// Importing the task.js model
const Task = require('../models/task');

// CONTROLLER FUNCTIONS for export to be used in the taskRoute.js file

// GET all tasks
module.exports.getAllTasks = () => {
    return Task.find({})
        .then(result => {
            return result;
        })
};

// POST to create a new task
// the requestBody is the req.body from the taskRoute.js file and passed as an argument
module.exports.createTask = (requestBody) => {
    // Creating a new task object on the Mongoose model Task
    let newTask = new Task({
        // Sets the name property of the new task object to the name property of the requestBody
        name: requestBody.name
    });

    // Saves the new task object to the database
    // The .then method waits until the new task object is saved to the database or an error occurs before returning a "true" or "false" value to the client/Postman
    return newTask.save().then((task, err) => {
        if (err) {
            console.log(err);
            return false;
        } else {
            return task;
        }
    });
};

// GET a specific task
module.exports.getTask = (taskId) => {
    return Task.findById(taskId).then((getSingleTask, err) => {
        if (err) {
            console.log(err);
            return false;
        } else {
            return getSingleTask;
        }
    })
};

// PUT to update a specific task
module.exports.updateTask = (taskId, newContent) => {
    return Task.findById(taskId).then((result, err) => {
        if (err) {
            console.log(err);
            return false;
        }

        result.name = newContent.name;
        return result.save().then((updatedTask, saveErr) => {
            if (saveErr) {
                console.log(saveErr);
                return false;
            } else {
                return updatedTask;
            }
        });
    });
};

// PUT to update a specific task STATUS to complete
module.exports.updateTaskStatus = (taskId, newContent) => {
    return Task.findById(taskId).then((result, err) => {
        if (err) {
            console.log(err);
            return false;
        }

        result.status = "complete";
        return result.save().then((updatedTask, saveErr) => {
            if (saveErr) {
                console.log(saveErr);
                return false;
            } else {
                return updatedTask;
            }
        });
    });
};


// DELETE to delete a specific task (not part of activity, personal addition)
module.exports.deleteTask = (taskId) => {
    return Task.findByIdAndDelete(taskId).then((removedTask, err) => {
        if (err) {
            console.log(err);
            return false;
        } else {
            return removedTask;
        }
    });
};