// Dependencies
const express = require('express');
const mongoose = require('mongoose');

// Accessing all the routes from the taskRoute.js file
const taskRoute = require('./routes/taskRoute');

// Creating an express app
const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Connecting to the database
mongoose.connect('mongodb+srv://jlumbajrph:Admin.123@test.ezjezum.mongodb.net/?retryWrites=true&w=majority', 
    { 
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);
  
// Using the /tasks routes created in the taskRoute.js file
app.use('/tasks', taskRoute);

// Listening to the port/server
if (require.main === module) {
    app.listen(port, () => {
        console.log(`Server is running on port: ${port}`);
    });
};

module.exports = app;