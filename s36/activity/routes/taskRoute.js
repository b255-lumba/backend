// Dependencies: express, taskController.js
const express = require('express');

// express Router instance to serve as the middleware for the /tasks route and as routing system
const router = express.Router();

// Importing the taskController.js file
const taskController = require('../controllers/taskController');

// SETTING UP THE ROUTES
// GET all tasks
router.get('/', (req, res) => {
    taskController.getAllTasks()
        .then(resultFromController => 
            res.send(resultFromController))
});

// POST to create a new task
router.post('/', (req, res) => {
    taskController.createTask(req.body)
        .then(resultFromController => 
            res.send(resultFromController))
});

// GET a specific task
router.get('/:id', (req, res) => {
    taskController.getTask(req.params.id)
        .then(resultFromController =>
            res.send(resultFromController))
});

// PUT to update a specific task STATUS to complete
router.put('/:id/complete', (req, res) => {
    taskController.updateTaskStatus(req.params.id)
        .then(resultFromController =>
            res.send(resultFromController))
});

// PUT to update a specific task to complete
router.put('/:id', (req, res) => {
    taskController.updateTask(req.params.id)
        .then(resultFromController =>
            res.send(resultFromController))
});

// DELETE to delete a specific task (not part of activity, personal addition)
router.delete('/:id', (req, res) => {
    taskController.deleteTask(req.params.id)
        .then(resultFromController =>
            res.send(resultFromController))
});

// Exporting the router
module.exports = router;
