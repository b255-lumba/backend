// [SECTION] Syntax, Statements and Comments
// Statements in programming are instructions that we tell the computer perform
// JS Statements usuall end with a semicolon (;)
// Comments are parts of the code that gets ignored by the language, Ctrl+"/" for single-line, Ctrl+Shift+"/" for multi-line
// Comments are meant to describe the written code

/*
	There are two types of comments:
	1. The single-line comment denoted by two slashes
	2. The multi-line comment denoted by a slash and an asterisk
*/

// [SECTION] Variables

// A variable is used to contain data
// Any information that is used by an application is stored in what we call a "memory"
// When we create variables, certain portions of a device's memory is give a "name" that we call "variables"

// Declaring Variables
// Declaring Variables - tells our device that a variable name is created and ready to store data
// Declaring a variable without assigning a value will automatically give it the value of "undefined"

let myVariable;
console.log(myVariable);
// console.log() is useful for printing out values of variables or certain results of code into the console

let hello;
console.log(hello);
// Varialbes must be declared first before they are used
// Using variables before they're called

/*
	Guides in writign variables:
	1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value
	2. Use camelCase for variableNames (lowercase for single word)
	3. For constant variable, use 'const' keyword
	4. Variable names should be indicative (or descriptive) of the value stored to avoid confusion
*/

// Declaring and initializing variables
// Initializing variables - the instance when a variable is given its initial/starting value

let productName = 'desktop computer';
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

// In the context of certain applications, some variables/information are constants and should not be changed
// In this example, the interest rate for a lone, savings account or a mortgage must not be changed due to real world concerns

const interest = 3.539;

// Reassigning variable values
// Reassigning a variable means changing its initial or previous value into another value

productName = 'Laptop';
console.log(productName);

// Reassigning variables vs initializing variables
// Declares a variable first
let supplier;
// Initialization is done after the variable has been declared
// This is considered as initialization because it is the first time that a value has been assigned to a variable

supplier = "John Smith Tradings"; // initialization
console.log(supplier);

// This is considered a reassignment because its initial value was already declared
supplier = 'Zuitt Store';
console.log(supplier);

// var v.s. let/const
// var is also used in declaring variables, but var is an ECMAScript (ES1) feature (JS 1997)
// let/const was introduced as a new feature in ES6 (2015)

// There are issues