// console.log("Hello friend")

// fetch request using GET method to retrieve all the TO-DO list items from JSON Placeholder API
fetch('https://jsonplaceholder.typicode.com/todos')
    // This line converts the fetched data in a json format:
.then(response => response.json())
    // With multiple "then" methods creates a "promise chain"
.then(data => {
    const titles = data.map(item => item.title);
    console.log(titles);
})
  .catch(error => console.error(error));

// fetch request using GET method to retrieve a single item
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then(data => {
    const title = data.title;
    const status = data.completed;
    console.log(data);
    console.log(`The item "${title}" on the list has a status of ${status}`);
})

// fetch request using POST method to create a new item
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',

	headers: {
		'Content-type': 'application/json',
	},

	body: JSON.stringify({
        completed: false,
		title: 'Created To Do List Item',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// fetch request using PUT method to update an item
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',

	headers: {
		'Content-type': 'application/json',
	},

	body: JSON.stringify({
        dateCompleted: "Pending",
        description: "To update the my to do list with a different data structure",
        status: "Pending",
		title: "Updated To Do List Item",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// fetch request using PATCH method to update an item
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',

	headers: {
		'Content-type': 'application/json',
	},

	body: JSON.stringify({
        dateCompleted: "04/02/2023",
        status: "Completed"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// fetch request using DELETE method to delete an item
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
})
