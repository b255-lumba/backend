/*
	
//Note: strictly follow the variable names and function names from the instructions.

*/

function addNum(num1, num2) {
	let sum = num1 + num2;
	console.log("Displayed sum of " + num1 + " and " + num2);
	console.log(sum);
}
addNum(5, 7);


function subNum(num1, num2) {
	let difference = num1 - num2;
	console.log("Displayed difference of " + num1 + " and " + num2);
	console.log(difference);
}
subNum(5, 7);


function multiplyNum(num1, num2) {
	console.log("The product of " + num1 + " and " + num2 +
	 ":");
	return num1 * num2; 
}
let product = multiplyNum(5, 7);
console.log(product);


function divideNum(num1, num2) {
	console.log("The quotient of " + num1 + " and " + num2 +
	 ":");
	return num1 / num2;
}
let quotient = divideNum(35, 5);
console.log(quotient);


function getCircleArea(radius) {
	console.log("The result of getting the area of a circle with " + radius + " radius:")
	return Math.PI * Math.pow(radius, 2);
}
let circleArea = getCircleArea(15);
console.log(circleArea);


function getAverage(num1, num2, num3, num4) {
	console.log("The average of " + num1 + ", " + num2 + ", " + num3 + ", and " + num4 + ":");
	return (num1 + num2 + num3 + num4) / 4;
}
let averageVar = getAverage(5, 7, 6, 6);
console.log(averageVar);


function checkIfPassed(score, totalScore) {
	console.log("Is " + score + "/" + totalScore + " a passing score?");
	let percentage = (score / totalScore) * 100;
	
	if (percentage >= 75) {
		let isPassed = true;
		return isPassed;
	} else {
		let isPassed = false;
		return isPassed;
	}
}
let isPassingScore = checkIfPassed(25, 50)
console.log(isPassingScore);




//Do not modify
//For exporting to test.js
try {
	module.exports = {
		addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
	}
} catch (err) {

}
