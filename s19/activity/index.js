console.log("Hello friend");
/*
    
    1. Create a function called login which is able to receive 3 parameters called username,password and role.
        -add an if statement to check if the the username is an empty string or undefined or if the password is an empty string or undefined or if the role is an empty string or undefined.
            -if it is, return a message in console to inform the user that their input should not be empty.
        -add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:
                -if the user's role is admin, return the following message:
                    "Welcome back to the class portal, admin!"
                -if the user's role is teacher,return the following message:
                    "Thank you for logging in, teacher!"
                -if the user's role is a rookie,return the following message:
                    "Welcome to the class portal, student!"
                -if the user's role does not fall under any of the cases, as a default, return a message:
                    "Role out of range."
*/
let username;
let password;
let role;

function login(username, password, role) {
	username = prompt("Enter your username: ").toLowerCase();
	password = prompt("Enter your password: ").toLowerCase();
	role = prompt("Enter your role: ").toLowerCase();
	console.log("login(" + username + ", " + password + ", " + role + ")");
	// template literals is not working

	if (username === "" || password === "" || role === "") {
		console.log("Inputs must not be empty");
		// alert("Inputs must not be empty");
	} else {
		switch (role) {
			case "admin":
				console.log("Welcome back to the class portal, admin!");
				// alert("Welcome back to the class portal, admin!");
				break;
			case "teacher":
				console.log("Thank you for logging in, teacher!");
				// alert("Thank you for logging in, teacher!");
				break;
			case "rookie":
				console.log("Welcome to the class portal, student!");
				// alert("Welcome to the class portal, student!");
				break;
			default:
				console.log("Role out of range.");
				// alert("Role out of range.");
				break;
		}
	}
}
login();

/*
    2. Create a function called checkAverage able to receive 4 numbers as arguments calculate its average and log a message for  the user about their letter equivalent in the console.
        -add parameters appropriate to describe the arguments.
        -create a new function scoped variable called average.
        -calculate the average of the 4 number inputs and store it in the variable average.
        -research the use of Math.round() and round off the value of the average variable.
            -update the average variable with the use of Math.round()
            -console.log() the average variable to check if it is rounding off first.
        -add an if statement to check if the value of average is less than or equal to 74.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is F"
        -add an else if statement to check if the value of average is greater than or equal to 75 and if average is less than or equal to 79.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is D"
        -add an else if statement to check if the value of average is greater than or equal to 80 and if average is less than or equal to 84.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is C"
        -add an else if statement to check if the value of average is greater than or equal to 85 and if average is less than or equal to 89.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is B"
        -add an else if statement to check if the value of average is greater than or equal to 90 and if average is less than or equal to 95.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is A"
        -add an else if statement to check if the value of average is greater than 96.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is A+"

        Invoke and add a number as argument using the browser console.

    Note: strictly follow the instructed function names.
*/
function checkAverage(num1, num2, num3, num4) {
	average = (num1 + num2 + num3 + num4) / 4;
	average = Math.round(average);
	// console.log("Hello student, your average is: " + average + ".");
	if (average <= 74) {
		console.log("Hello student, your average is: " + average + ". The letter equivalent if F");
	} else if (average >=75 && average <= 79) {
		console.log("Hello student, your average is: " + average + ". The letter equivalent if D");
	} else if (average >= 80 && average <= 84) {
		console.log("Hello student, your average is: " + average + ". The letter equivalent if C");
	} else if (average >= 85 && average <= 89) {
		console.log("Hello student, your average is: " + average + ". The letter equivalent if B");
	} else if (average >= 90 && average <= 95) {
		console.log("Hello student, your average is: " + average + ". The letter equivalent if A");
	} else if (average <= 96) {
		console.log("Hello student, your average is: " + average + ". The letter equivalent if A+");
	}
}


//Do not modify
//For exporting to test.js
try {
    module.exports = {
       login, checkAverage
    }
} catch(err) {

}
