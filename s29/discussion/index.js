// [SECTION] Comparison Operators

// $gt = greater than
// $gte = greater than or equal to

db.users.find({age : { $gt : 50}}).pretty(); // .pretty() optional

db.users.find({age : { $gte : 50}}).pretty(); // .pretty() optional

// $lt = less than
// $lte = less than or equal to

db.users.find({age : { $lt : 50}});

db.users.find({age : { $lte : 50}}); 

// $ne = not equal
// Allows us to find documents that have field number values not equal to a specified value (exclude specific value)

db.users.find({age : { $ne : 82}});

// $in
// Allows us to find docs with specific match criteria one field using different values

db.users.find({lastname : { $in : ["Hawking", "Doe"]}});
db.users.find({name : { $in : ["HTML 101", "CSS 101"]}});

// [SECTION] Logical Query Operators

// $or
// Allows us to find documents that match a single criteria from multiple provided search criteria (search for matches on each criteria)

db.users.find({ $or : [{firstName: "Neil"}, {age: "25"}]});

db.users.find({ $or : [{firstName: "Neil"}, {age: {$gt : 70}}]});

// $and
// Allows us to find documents matching multiple criteria (all criterias met)

db.users.find({ $and : [{age: {$ne: 82}, {$ne: 76}}]});

// [SECTION] Field Projection
/*
	- Retrieving documents are common operations that we do by default. MongoDB queries return the whole document as a response.
	- When dealing with complex data structures, there might be instances when fields are not useful for the query we are trying to accomplish.
	- To help with readability of the values returned, we can include/exclude fields from the response (such as sensitive informations)
*/

// Exclusion: 0, Inclusion: 1;

db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		email: 1
	}
);

db.users.find(
	{	
		firstName: "Jane"
	},
	{
		company: 0,
		email: 0
	}
);

// Supressing the ID field
/*
	- Allows us to exclude the "_id" field when retrieving documents.
	- When using field projection, field inclusion and exclusion may not be used at the same time.
	- Excluding the "_id" field is the only exception to this rule.
*/

db.users.find(
	{	
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		company: 1,
		_id: 0
	}
);

db.users.insert({
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    contact: {
        phone: "87654321",
        email: "johnsmith@gmail.com"
    },
    courses: [ "CSS", "Javascript", "Python" ],
    department: "none"
});

// To access embedded documents
db.users.find(
	{	
		firstName: "John"
	},
	{
		firstName: 1,
		lastName: 1,
		"contact.phone": 1
	}
);

// Suppressing specific fields in embedded documents
db.users.find(
	{	
		firstName: "John"
	},
	{
		"contact.phone": 0
	}
);

db.users.insert({
    namearr: [
    	{
    		namea: "juan"	
    	},
    	{
    		nameb: "tamad"
    	}

    ]
});

// Project specific array elements in the returned array
// The $slice operator allows us to retrieve only 1 element that matches the search criteria

db.users.find(
	{	"namearr":
		{
			namea: "juan"
		}
	},
	{
		namearr: {$slice: 1}
	}

);

// $regex operator
/*
	- Allows us to find documents that match a specific string pattern using regular expressions.
*/

// Case Sensitive Query
db.users.find({firstName: {$regex: 'N'}});

// Case Insensitive Query
db.users.find({firstName: {$regex: 'j'}, $options: '$i'});
